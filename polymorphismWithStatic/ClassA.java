package polymorphismWithStatic;

/**
 *
 * @author qqq4343
 */
public class ClassA {
    String var1 = "var1 in A";
    int num = 1;
    static String var2 = "var2 in A";
    static String var3 = "var2 in A";
    
    public void m1(){
        System.out.println("m1 in A");
    }
    public static void m2(){
        System.out.println("m2 in A");
    }
}
