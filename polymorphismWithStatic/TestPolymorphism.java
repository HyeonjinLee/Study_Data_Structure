package polymorphismWithStatic;

/**
 *
 * @author qqq4343
 */
public class TestPolymorphism {
    public static void main(String[] args) {
        ClassA a = new ClassB();
        a.m2();//look at the variable type
        a.m1();//look at the variable initialization
       
        System.out.println("a.var1 : " + a.var1);
        System.out.println("a.var2 : " + a.var2);
        System.out.println("a.num : " + a.num);
        
        ClassB b = new ClassB();
       
        System.out.println("b.var1 : " + b.var1);
        System.out.println("b.var2 : " + b.var2);
        System.out.println("b.num : " + b.num);
    }
}
