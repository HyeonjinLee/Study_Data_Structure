package polymorphismWithStatic;

/**
 *
 * @author qqq4343
 */
public class ClassB extends ClassA {
    String var1 = "var1 in B";
    int num = 5;
    static String var2 = "var2 in B";
    
    public void m1(){
        System.out.println("m1 in B");
    }
    public static void m2(){
        System.out.println("m2 in B");
    }
}
