package staicNnonstatic;

/**
 *
 * @author qqq4343
 */
public class TestStatic {

    public static void main(String[] args) {
        Student jon = new Student();
        jon.name = "jon";
        jon.id = "111222";
        jon.grade = "A";
        jon.printInfo();
        
        Student mark = new Student();
        mark.name = "Mark";
        mark.id = "222333";
        mark.setGrade("B");
        mark.printInfo();
        
        Student kim = new Student();
        kim.name = "Kim";
        kim.id = "333444";
        kim.grade ="C";
        kim.printInfo();
        
        System.out.println();
        System.out.println();
        System.out.println("Reprinting Info");
        jon.printInfo();
        mark.printInfo();
        kim.printInfo();
    }

}
