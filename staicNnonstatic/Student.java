package staicNnonstatic;

/**
 *
 * @author qqq4343
 */
public class Student {
    public  String name;
    public  String id;
    public static String grade;

    public static void setGrade(String par_grade) {
        grade = par_grade;
    }
    
    public void printInfo(){
        System.out.println("Name: " + name + ",ID: " + id +", Grade: " + grade);
    }
}
