package recursion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author qqq4343
 */
public class Recursion {
    
    static int numFiles = 1;
    static int numFolders = 2;
    
    static int numBreadthFiles = 1;
    static int numBreadthFolders = 2;
    
    public static void main(String[] args) {
        try {
            File f1 = new File("Folder1");
            f1.mkdir();
            generateFile(f1);
            listFiles(f1);
            deleteFilesFolders(f1);
            File f2 = new File("BreadthFolder1");
            f2.mkdir();
            File[] breadthFolderList = {f2};
            generateFilesBreadthRecursion(breadthFolderList);
        } catch (Exception e) {
        }
    }
    /**
     * Crate a file/folder structure where each folder contains 3 files
     * and 1 folder.
     * 
     * @param foldername
     * @throws IOException 
     */
    public static void generateFile(File foldername) throws IOException {
       if (numFiles > 50 || numFolders > 50) {
           return;
       }
       for (int i = 0 ; i < 3; i++){
           File a = new File(foldername.getAbsolutePath()+"\\File"+numFiles+".txt");
           a.createNewFile();
           numFiles++;
       }
       File a = new File(foldername.getAbsolutePath()+"\\Folder"+numFolders);
       a.mkdir();
       numFolders++;
       generateFile(a);
    }
    /**
     * List all the files within a specified directory.
     * 
     * @param file The directory name.
     * @throws IOException If an error exists accessing file information.
     */
    public static void listFiles(File file) throws IOException {
        if(file.isFile()){
            System.out.println(file.getName());
        }
        if (file.isDirectory()){
            File[] files = file.listFiles();
            for (File f:files){
                listFiles(f);
            }
        }
    }
    /**
     * Deletes all the files and folders in a specified directory.
     * 
     * @param file 
     * @throws java.io.IOException 
     */
    public static void deleteFilesFolders(File file) throws IOException {
        if (file.isDirectory()){
            File[] files = file.listFiles();
            for (File f : files){
                deleteFilesFolders(f);
            }
        }
        file.delete();
    }
    /**
     * Generates a file structure where each folder has 3 files and 3 folders.
     * Files are generated horizontally as the recursive algorithm moves down layer
     * by layer. In contrast to depth recursion that generates a file structure vertically 
     * to the maximum depth, then moves 1 node horizontally.
     * 
     * @param foldernames
     * @throws IOException 
     */
    public static void generateFilesBreadthRecursion(File[] foldernames) throws IOException {
        if (numBreadthFiles > 100 || numBreadthFolders > 100){
            return;
        }
        ArrayList<File> newFolders = new ArrayList();
        for (File folder : foldernames) {
            for (int i = 0; i < 3; i++) {
                File a = new File(folder.getAbsolutePath()+"\\BreadthFile"
                        +numBreadthFiles+".txt");
                a.createNewFile();
                numBreadthFiles++;
            }
            for (int i = 0; i < 3; i++){
                File a = new File(folder.getAbsoluteFile()+"\\BreadthFolder"+numBreadthFolders);
                a.mkdir();
                numBreadthFolders++;
                newFolders.add(a);
            }
        }
        File[] newFolderNames = new File[newFolders.size()];
        newFolders.toArray(newFolderNames);
        generateFilesBreadthRecursion(newFolderNames);
    }
}
