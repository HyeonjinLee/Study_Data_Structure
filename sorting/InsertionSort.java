package sorting;

/**
 *
 * @author qqq4343
 */
public class InsertionSort {

    public static void insertionSort(int[] list) {
        for (int i = 0; i < list.length; i++) {
            int currentElement = list[i];
            int k;
            for (k=i-1; k >= 0 && list[k] > currentElement; k--){
                System.out.println("cur : " + currentElement);
                System.out.println("list[k] : "+list[k]);
                System.out.println("list[k+1] : "+list[k+1]);
                list[k+1] = list[k];
            }       
            list[k+1] = currentElement;
        }
    }
    
    public static void main(String[] args) {
        int[] list = {1,9,4,6,5,-4};
        insertionSort(list);
        for (int i : list){
            System.out.print(i+", ");
        }
    }
}
