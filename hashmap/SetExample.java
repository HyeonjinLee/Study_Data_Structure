package hashmap;


import java.util.*;
public class SetExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashSet<String> nameSet = new HashSet();
        nameSet.add("Mike");
        nameSet.add("Tom");
        nameSet.add("Kim");
        nameSet.add("Tom");
        nameSet.add("Michael");
        nameSet.add("Alice");
        nameSet.add("Tom");
        nameSet.add("Alice");
        nameSet.add("Tom");
        nameSet.add("Jonathan");
        nameSet.add("mop");
        
        System.out.println("The Set of names is:");
        System.out.println(nameSet);
       
        System.out.println("The size of the list is:");
        System.out.println(nameSet.size());
        
        
        HashSet<String> objectSet = new HashSet();
        objectSet.add("mop");
        objectSet.add("mice");
        objectSet.add("mango");
        objectSet.add("macaron");
        objectSet.add("magnet");
        objectSet.add("milk");
        objectSet.add("moon");
        objectSet.add("mushroom");
        objectSet.add("mango");
        objectSet.add("morning");
        
        System.out.println("The Set of objects is:");
        System.out.println(objectSet);
        
        HashSet<String> workSet = new HashSet();
        System.out.println("The work set is:");
        System.out.println(workSet);
        System.out.println("Add all of names and objects into work set");
        workSet.addAll(nameSet);
        workSet.addAll(objectSet);
        System.out.println(workSet);
        
        System.out.println("Iterate over the work set:");
        System.out.println("Using Iterator");
        Iterator<String> i = workSet.iterator();
        while (i.hasNext())
        {
            System.out.println("  -> " + i.next());
        }
        System.out.println();
        System.out.println("Using For Each");
        for (String s: workSet)
        {
            System.out.println("  *-> " + s);
        }
        
        System.out.println();
        System.out.println("Intersection of both sets");
        System.out.println("Suggestion 1");
        workSet.clear();
        for (String s: nameSet)
        {
            if (objectSet.contains(s))
            {
                workSet.add(s);
            }
        }
        System.out.println(workSet);
        
        System.out.println();
        System.out.println("Suggestion 2");
        workSet.clear();
        workSet.addAll(nameSet);
        workSet.addAll(objectSet);
        workSet.retainAll(nameSet);
        workSet.retainAll(objectSet);
        System.out.println(workSet);
        
        
        
        System.out.println();
        System.out.println("TreeSet of nameSet");
        TreeSet<String> nameTreeSet = new TreeSet();
        nameTreeSet.addAll(nameSet);
        System.out.println(nameTreeSet);
        System.out.println("nameTreeSet in descending order");
        //nameTreeSet.descendingSet();
        System.out.println(nameTreeSet.descendingSet());
        
        System.out.println("Higher than Michael");
        System.out.println(nameTreeSet.higher("Michael"));
        System.out.println("Lower than Michael");
        System.out.println(nameTreeSet.lower("Michael"));
        
        
        System.out.println("Head Set  Michael");
        System.out.println(nameTreeSet.headSet("Michael"));
        System.out.println("Tail Set Michael");
        System.out.println(nameTreeSet.tailSet("Michael"));
        
        System.out.println(nameTreeSet);
        System.out.println("Polling a set");
        System.out.println("Poll First");
        System.out.println(nameTreeSet.pollFirst());
        System.out.println("Poll Last");
        System.out.println(nameTreeSet.pollLast());
        System.out.println("nameTreeSet is now:");
        System.out.println(nameTreeSet);
    }
    
}
