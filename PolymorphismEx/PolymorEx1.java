package PolymorphismEx;

/**
 *
 * @author qqq4343
 */
public class PolymorEx1 {
    public static void main(String[] args) {
        Deer d = new Deer();
        Animal a = d;
        Vegetarian v = d;
        Object o = d;
    }
}

